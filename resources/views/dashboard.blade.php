@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="col-md-3">
                        <ul class="list-group">
                            <a href="/dashboard/orders" class="list-group-item">Orders</a>
                            <a href="/dashboard/users" class="list-group-item">Users</a>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        @yield('page')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
