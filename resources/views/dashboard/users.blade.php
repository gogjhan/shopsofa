@extends('dashboard')

@section('page')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Users</h3>
        </div>
        <div class="panel-body">
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#customers" aria-controls="customers" role="tab" data-toggle="tab">Customers</a></li>
                    <li role="presentation"><a href="#managers" aria-controls="managers" role="tab" data-toggle="tab">Sales Managers</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="customers">
                        <div class="row">
                            <div class="col-md-12 clearfix">
                                <a href="{{ route('users.export-data') }}" class="btn btn-primary pull-right" style="color:white; margin: 10px 0;">Export Data</a>
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>
                                        <a href="users/{{$user->id}}" class="btn btn-default">Orders</a>
                                        <li class="dropdown" style="list-style-type: none; display:inline-block;">
                                            <a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-expanded="false">
                                                <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="/dashboard/users/{{$user->id}}/make-sales-manager">Make Sales Manager</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="managers">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($salesManagers as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>
                                        <a href="users/{{$user->id}}" class="btn btn-default">Orders</a>
                                        <li class="dropdown" style="list-style-type: none; display:inline-block;">
                                            <a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-expanded="false">
                                                <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="/dashboard/users/{{$user->id}}/make-customer">Make Customer</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection