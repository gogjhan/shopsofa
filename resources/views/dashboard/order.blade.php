@extends('dashboard')

@section('page')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Order #{{$order->id}}</h3>
        </div>
        <div class="panel-body">
            <div>

                <ol class="breadcrumb">
                    <li><a href="/dashboard/orders">Orders</a></li>
                    <li class="active">{{$order->id}}</li>
                </ol>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#order" aria-controls="order" role="tab" data-toggle="tab">Order Details</a></li>
                    <li role="presentation"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">User Details</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="order" style="padding: 20px;">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Order ID</th>
                                <td>{{ $order->id }}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $order->name }}</td>
                            </tr>
                            <tr>
                                <th>Color</th>
                                <td>{{ $order->color }}</td>
                            </tr>
                            <tr>
                                <th>Fabric</th>
                                <td>{{ $order->fabric }}</td>
                            </tr>
                            <tr>
                                <th>No. of seats</th>
                                <td>{{ $order->num_seats }}</td>
                            </tr>
                            <tr>
                                <th>Piping</th>
                                <td>{{ $order->piping }}</td>
                            </tr>
                            <tr>
                                <th>Legs</th>
                                <td>{{ $order->legs }}</td>
                            </tr>
                            <tr>
                                <th>Foam Density Back</th>
                                <td>{{ $order->foam_density_back }}</td>
                            </tr>
                            <tr>
                                <th>Foam Density Seat</th>
                                <td>{{ $order->foam_density_seat }}</td>
                            </tr>
                            <tr>
                                <th>Height</th>
                                <td>{{ $order->height }}</td>
                            </tr>
                            <tr>
                                <th>Width</th>
                                <td>{{ $order->width }}</td>
                            </tr>
                            <tr>
                                <th>Depth</th>
                                <td>{{ $order->depth }}</td>
                            </tr>
                            <tr>
                                <th>Date</th>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="user" style="padding:20px;">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>User ID</th>
                                <td>{{$order->user->id}}</td>
                            </tr>
                            <tr>
                                <th>First Name</th>
                                <td>{{$order->user->first_name}}</td>
                            </tr>
                            <tr>
                                <th>Last Name</th>
                                <td>{{$order->user->last_name}}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>{{$order->user->mobile}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$order->user->email}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{$order->user->address}}</td>
                            </tr>
                            <tr>
                                <th>City</th>
                                <td>{{$order->user->city}}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{$order->user->country}}</td>
                            </tr>
                            <tr>
                                <th>Info1</th>
                                <td>{{$order->user->info1}}</td>
                            </tr>
                            <tr>
                                <th>Info2</th>
                                <td>{{$order->user->info2}}</td>
                            </tr>
                            <tr>
                                <th>Info3</th>
                                <td>{{$order->user->info3}}</td>
                            </tr>
                            <tr>
                                <th>Join Date</th>
                                <td>{{$order->user->created_at}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection