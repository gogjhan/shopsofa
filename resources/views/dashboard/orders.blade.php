@extends('dashboard')

@section('page')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Orders</h3>
        </div>
        <div class="panel-body">
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#submitted" aria-controls="submitted" role="tab" data-toggle="tab">Submitted</a></li>
                    <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved for later</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="submitted">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>User Name</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($submittedOrders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->first_name }} {{ $order->user->last_name }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>
                                        <a href='orders/{{$order->id}}' class="btn btn-default">
                                            Details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="saved">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>User Name</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($savedOrders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->first_name }} {{ $order->user->last_name }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>
                                        <a href='orders/{{$order->id}}' class="btn btn-default">
                                            Details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection