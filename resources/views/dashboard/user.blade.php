@extends('dashboard')

@section('page')
    <div class="panel panel-default">
    	  <div class="panel-heading">
    			<h3 class="panel-title">User Info</h3>
    	  </div>
    	  <div class="panel-body">
              <ol class="breadcrumb">
                  <li><a href="/dashboard/users">Users</a></li>
                  <li class="active">{{$user->first_name}} {{$user->last_name}}</li>
              </ol>

              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title clearfix">
                          Orders
                          <a href="{{ route('users.orders.export-data', ['userId' => $user->id]) }}" class="btn btn-primary pull-right" style="color:white">Export Data</a>
                      </h3>
                  </div>
                  <div class="panel-body">
                      <div>

                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#submitted" aria-controls="submitted" role="tab" data-toggle="tab">Submitted</a></li>
                              <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved for later</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active table-responsive" id="submitted">
                                  <table class="table">
                                      <thead>
                                      <tr>
                                          <th>Order ID</th>
                                          <th>Name</th>
                                          <th>Color</th>
                                          <th>Fabric</th>
                                          <th>No. of Seats</th>
                                          <th>Piping</th>
                                          <th>Legs</th>
                                          <th>Foam Density Back</th>
                                          <th>Foam Density Seat</th>
                                          <th>Height</th>
                                          <th>Width</th>
                                          <th>Depth</th>
                                          <th>Date</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($submittedOrders as $order)
                                          <tr>
                                              <td>{{ $order->id }}</td>
                                              <td>{{ $order->name }}</td>
                                              <td>{{ $order->color }}</td>
                                              <td>{{ $order->fabric }}</td>
                                              <td>{{ $order->num_seats }}</td>
                                              <td>{{ $order->piping }}</td>
                                              <td>{{ $order->legs }}</td>
                                              <td>{{ $order->foam_density_back }}</td>
                                              <td>{{ $order->foam_density_seat }}</td>
                                              <td>{{ $order->height }}</td>
                                              <td>{{ $order->width }}</td>
                                              <td>{{ $order->depth }}</td>
                                              <td>{{ $order->created_at }}</td>
                                          </tr>
                                      @endforeach
                                      </tbody>
                                  </table>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="saved">
                                  <div class=".table-responsive">
                                      <table class="table">
                                          <thead>
                                          <tr>
                                              <th>Order ID</th>
                                              <th>Name</th>
                                              <th>Color</th>
                                              <th>Fabric</th>
                                              <th>No. of Seats</th>
                                              <th>Piping</th>
                                              <th>Legs</th>
                                              <th>Foam Density</th>
                                              <th>Height</th>
                                              <th>Width</th>
                                              <th>Date</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          @foreach($savedOrders as $order)
                                            <tr>
                                                  <td>{{ $order->id }}</td>
                                                  <td>{{ $order->name }}</td>
                                                  <td>{{ $order->color }}</td>
                                                  <td>{{ $order->fabric }}</td>
                                                  <td>{{ $order->num_seats }}</td>
                                                  <td>{{ $order->piping }}</td>
                                                  <td>{{ $order->legs }}</td>
                                                  <td>{{ $order->foam_density }}</td>
                                                  <td>{{ $order->height }}</td>
                                                  <td>{{ $order->width }}</td>
                                                  <td>{{ $order->created_at }}</td>
                                            </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
    	  </div>
    </div>
@endsection