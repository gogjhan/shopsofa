<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .logo
        {
            width: 150px;
        }

        .container > h1
        {
            font-size: 20px;
            width: 100%;
            text-align: center;
            border: 1px solid black;
        }

        .container > h1 > span {
            float: right;
            width: 200px;
            border-left: 1px solid black;
            text-align: left;
            font-size: 16px; font-weight: normal;
            padding: 2px;
            padding-left: 10px;
        }

        .container > form
        {
            border: 1px solid black;
            padding: 10px 0;
        }

        .form-group
        {
            display: block;
            clear: both;
            padding: 10px;
        }

        .form-group > label {
            width: 20%; float: left;
        }

        .form-group > span {
            width: 80%; float: left;
            border-bottom: dotted;
        }

        .form-group > span:after {
            content: "s";
            color: white;
        }

        .clearfix:after {
            content: ".";
            visibility: hidden;
            display: block;
            height: 0;
            clear: both;
        }

        .images {  width: 100%; margin-top: 20px;}
        .image-container {float: left; box-sizing:border-box;}
        .image { border: 1px solid black; display: block;}

        .see-you-soon {
            margin-top: 20px;
            width: 100%;
            font-weight: bolder;
            font-size: 18px;
            text-align: center;
        }

        .footer
        {
            width: 100%; clear:both;
            position: absolute; bottom: 0;
            text-align:center;  font-size: 10px; color: #757575;
        }
    </style>
</head>
<body>

<div class="container">
    <img src="images/logo.png" class="logo">

    <h1 class="clearfix">WISH LIST <span>DATE: {{ $order['date'] }}</span></h1>

    <form class="clearfix">
        <div class="form-group">
            <label>NAME:</label>
            <span>{{ $order['name'] }}</span>
        </div>

        <div class="form-group">
            <label>MOBILE:</label>
            <span>{{ $order['mobile'] }}</span>
        </div>

        <div class="form-group">
            <label>EMAIL:</label>
            <span>{{ $order['email'] }}</span>
        </div>

        <div class="form-group">
            <label>MODEL:</label>
            <span>{{ $order['model'] }}</span>
        </div>

        <div class="form-group">
            <label>SIZE:</label>
            <span>{{ $order['size'] }}</span>
        </div>

        <div class="form-group">
            <label>QUANTITY:</label>
            <span>{{ $order['quantity'] }}</span>
        </div>

        <div class="form-group">
            <label>FABRICS:</label>
            <span>{{ $order['fabrics'] }}</span>
        </div>

        <div class="form-group">
            <label>FOAM DENSITY:</label>
            <span>{{ $order['foam_density'] }}</span>
        </div>

        <div class="form-group">
            <label>LEGS:</label>
            <span>{{ $order['legs'] }}</span>
        </div>
    </form>

    <form class="clearfix" style="margin-top: 10px; padding-top: 0;">
        <div class="form-group">
            <label>PRICE:</label>
            <span>{{ $order['price'] }}</span>
        </div>
    </form>

    <div class="images clearfix">
        <div class="image-container" style="width: 60%;">
            <img class="image" src="{{ asset('images/textures/'.$order['texture_source']) }}" style="height: 300px; width: 100%;">
        </div>
        <div class="image-container" style="width: 40%; padding-left: 5px;">
            <div class="image" style="height: 150px; width: 100%;"></div>
            <div class="image" style="height: 150px; width: 100%;margin-top: 5px;"></div>
        </div>
    </div>

    <form class="clearfix" style="margin-top: 10px; padding-top: 0;">
        <div class="form-group">
            <label>OFFER VALIDITY:</label>
            <span>{{ $order['offer_validity'] }}</span>
        </div>
    </form>

    <div class="see-you-soon">
        See you soon...
    </div>

    {{--<div class="form-group" style="position: absolute; bottom: 80px; width: 100%;">--}}
    {{--<label style="width: 10%;">DATE:</label>--}}
    {{--<span style="width: 30%;">{{ $order->created_at }}</span>--}}
    {{--</div>--}}
</div>

<div class="footer">
    <b>SOFABULOUS DOMESTIC FURNITURE TRADING L.L.C</b><br>
    A limited liability company incorporated pursuant to the laws of Dubai, United Arab Emirates operating under Trade License number 772724<br>
    Shop 16 Ground Floor API 1000 Al Thanya Street P.O. Box 213102 Dubai – U.A.E.
</div>
</body>
</html>