<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style>
        body, html, .container {
            padding: 0; margin: 0;
        }

        .container {
            max-width: 940px;
            margin-right: auto;
            margin-left: auto;
        }
        @media (min-width: 600px) {
            .grid {
                display: table;
                width: 100%;
                table-layout: fixed;
            }

            .col {
               display: table-cell;
            }

           .grid-padded {
               margin-left:  -1rem;
               margin-right: -1rem;
           }
            .grid-padded .grid {
                border-spacing: 1rem 0;
            }
        }
        @media (min-width: 600px) {
            .col-1 { width: 8.333333%; }
            .col-2 { width: 16.666667%; }
            .col-3 { width: 25%; }
            .col-4 { width: 33.333333%; }
            .col-5 { width: 41.666667%; }
            .col-6 { width: 50%; }
            .col-7 { width: 58.333333%; }
            .col-8 { width: 66.666667%; }
            .col-9 { width: 75%; }
            .col-10 { width: 83.333333%; }
            .col-11 { width: 91.666667%; }
        }

        .grid-align-middle .col {
            vertical-align: middle;
        }

        .grid-reverse {
            direction: rtl;
        }

        .grid-reverse .col {
            direction: ltr;
        }

        .product-list .col {
            border: 1px solid black;
            padding: 5px;
            border-bottom: none;
            border-right: none;
        }
        .product-list .col.th { font-weight: bold; }
        .product-list {
            border-right: 1px solid black;
        }

        .bordered { border: 1px solid black;}
        .bold { font-weight: bold;}
    </style>
</head>

<body style="font-size: 12px;">
<div class="container" style="padding: 20px 80px;">
    {{--LOGO--}}
    <div class="grid">
        <div class="col col-2">
            <img src="{{ asset('images/logo.png') }}" width="100%">
        </div>
        <div class="col col-10"></div>
    </div>

    {{--SLOGAN--}}
    <div class="grid">
        <div class="col col-2">
            <p style="text-align:center; font-size: 12px; ">Design it your way</p>
        </div>
        <div class="col col-10"></div>
    </div>

    {{--JOB ORDER / INVOICE--}}
    <div class="grid">
        <div class="col col-12">
            <h1 style="border: 1px solid black; text-align:center; font-weight: bold; font-size: 18px; padding: 2px 0;">
                JOB ORDER / INVOICE
            </h1>
        </div>
    </div>

    {{--ORDER DETAILS--}}
    <div class="grid">
        <div class="col col-6" style="border: 1px solid black; padding: 5px;">
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-3">
                    NAME:
                </div>
                <div class="col col-9" style="border-bottom: 1px dotted black">
                    {{ $order['name'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-3">
                    MOBILE:
                </div>
                <div class="col col-9" style="border-bottom: 1px dotted black">
                    {{ $order['mobile'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-3">
                    ADDRESS:
                </div>
                <div class="col col-9" style="border-bottom: 1px dotted black">
                    {{ $order['address'] }}
                </div>
            </div>
            <div class="grid">
                <div class="col col-3">
                    EMAIL:
                </div>
                <div class="col col-9" style="border-bottom: 1px dotted black">
                    {{ $order['email'] }}
                </div>
            </div>
        </div>
        <div class="col col-6" style="border: 1px solid black; padding: 5px; border-left: none;">
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-5">
                    JOB ORDER NUMBER:
                </div>
                <div class="col col-7" style="border-bottom: 1px dotted black">
                    {{ $order['job_order_number'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-6">
                    CUSTOMER REFERENCE:
                </div>
                <div class="col col-6" style="border-bottom: 1px dotted black">
                    {{ $order['customer_references'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-4">
                    ORDER DATE:
                </div>
                <div class="col col-8" style="border-bottom: 1px dotted black">
                    {{ $order['order_date'] }}
                </div>
            </div>
            <div class="grid">
                <div class="col col-5">
                    DELIVERY DATE:
                </div>
                <div class="col col-7" style="border-bottom: 1px dotted black">
                    {{ $order['delivery_date'] }}
                </div>
            </div>
        </div>
    </div>

    {{--ORDER LIST--}}
    <div class="product-list">
        {{--HEADINGS--}}
        <div class="grid" style="margin-top: 10px;">
            <div class="th col col-1">S. NO</div>
            <div class="th col col-4">DESCRIPTION</div>
            <div class="th col col-2">QUANTITY</div>
            <div class="th col col-2">UNIT PRICE</div>
            <div class="th col col-3">TOTAL AMOUNT</div>
        </div>
        {{--CONTENT--}}
        @foreach($order['items'] as $item)
            <div class="grid">
                <div class="td col col-1">{{ $item[0] }}</div>
                <div class="td col col-4">{{ $item[1] }}</div>
                <div class="td col col-2">{{ $item[2] }}</div>
                <div class="td col col-2">{{ $item[3] }}</div>
                <div class="td col col-3">{{ $item[4] }}</div>
            </div>
        @endforeach

        {{--FOOTER--}}
        <div class="grid" style="border-bottom: 1px solid black;">
            <div class="th col col-1"></div>
            <div class="th col col-4">TOTAL</div>
            <div class="th col col-2">{{ $order['total_quantity'] }}</div>
            <div class="th col col-2"></div>
            <div class="th col col-3">{{ $order['total_amount'] }}</div>
        </div>
    </div>

    {{--AMOUNT IN WORDS--}}
    <div class="grid bordered" style="margin-top: 10px;">
        <div class="col col-3 bold" style="border-right: 1px solid black; padding: 5px;">Amount In Words:</div>
        <div class="col col-9" style="padding: 5px 0 0 10px">{{ $order['amount_in_words'] }}</div>
    </div>

    {{--IMAGE CONTAINER--}}
    <div class="grid" style="margin-top: 10px;">
        <div class="col col-7 bordered" style="height: 300px;">
            <img src="{{ asset('images/textures/'.$order['texture_source']) }}" width="100%">
        </div>
        <div class="col col-5">
            <div class="grid" style="padding-left: 10px; padding-bottom: 10px;">
                <div class="col col-12 bordered" style="height: 150px;">

                </div>
            </div>
            <div class="grid" style="padding-left: 10px;">
                <div class="col col-12 bordered" style="height: 140px;">

                </div>
            </div>
        </div>
    </div>

    <div class="grid bordered" style="margin-top: 10px; padding: 5px;">
        <div class="col col-2">PAYMENT BY:</div>
        <div class="col col-4" style="position:relative;">
            <input type="checkbox" style="position: absolute;top: -5px;" {{ $order['cash'] === 'true' ? 'checked' : ''}}>
            <label style="padding-left: 20px;">CASH</label>
        </div>
        <div class="col col-4" style="position:relative;">
            <input type="checkbox" style="position: absolute;top: -5px;" {{ $order['cash'] !== 'true' ? 'checked' : ''}}>
            <label style="padding-left: 20px;">CREDIT CARD</label>
        </div>
    </div>

    {{--ORDER DETAILS--}}
    <div class="grid" style="margin-top: 10px;">
        <div class="col col-6" style="border: 1px solid black; padding: 5px;">
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-5">
                    PREPARED BY:
                </div>
                <div class="col col-7" style="border-bottom: 1px dotted black">
                    {{ $order['prepared_by'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-6">
                    DATE & SIGNATURE:
                </div>
                <div class="col col-6" style="border-bottom: 1px dotted black">

                </div>
            </div>
        </div>
        <div class="col col-6" style="border: 1px solid black; padding: 5px; border-left: none;">
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-5">
                    CONFIRMED BY:
                </div>
                <div class="col col-7" style="border-bottom: 1px dotted black">
                    {{ $order['confirmed_by'] }}
                </div>
            </div>
            <div class="grid" style="margin-bottom: 10px;">
                <div class="col col-6">
                    DATE & SIGNATURE:
                </div>
                <div class="col col-6" style="border-bottom: 1px dotted black">

                </div>
            </div>
        </div>
    </div>

    {{--FOOTER--}}
    <div class="grid" style="position: absolute; bottom: 40px;text-align:center;  font-size: 10px; color: #757575;">
        <div class="col col-12">
            <b>SOFABULOUS DOMESTIC FURNITURE TRADING L.L.C</b><br>
            A limited liability company incorporated pursuant to the laws of Dubai, United Arab Emirates operating under Trade License number 772724<br>
            Shop 16 Ground Floor API 1000 Al Thanya Street P.O. Box 213102 Dubai – U.A.E.
        </div>
    </div>
</div>
</body>
</html>