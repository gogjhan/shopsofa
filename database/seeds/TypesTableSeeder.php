<?php

use App\Type;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create(['name' => 'Admin']);
        Type::create(['name' => 'Customer']);
        Type::create(['name' => 'Sales Manager']);
    }
}
