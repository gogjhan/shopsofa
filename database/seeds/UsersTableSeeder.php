<?php

use App\Type;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'type_id' => Type::where('name', 'Admin')->first()->id,
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'mobile' => '123456',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
