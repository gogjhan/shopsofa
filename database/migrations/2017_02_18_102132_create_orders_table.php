<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('device_id')->nullable();
            $table->string('name');
            $table->integer('texture_id')->unsigned()->nullable();
            $table->foreign('texture_id')->references('id')->on('textures')->onDelete('cascade');
            $table->string('color');
            $table->string('fabric');
            $table->string('num_seats');
            $table->string('piping')->nullable();
            $table->string('legs')->nullable();
            $table->string('foam_density_back')->nullable();
            $table->string('foam_density_seat')->nullable();
            $table->string('sofa_quantity')->nullable();
            $table->string('height')->nullable();
            $table->string('width')->nullable();
            $table->string('depth')->nullable();
            $table->boolean('ordered')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
