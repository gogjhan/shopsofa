<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Order routes
 */
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('user',          'AdminController@getUser');
    Route::post('user/edit',    'AdminController@editUser');

    Route::group(['prefix' => 'orders'], function() {
        Route::get('',                  'OrderController@getAllOrders');
        Route::get('{id}',              'OrderController@getSingleOrder');
        Route::post('save',             'OrderController@saveOrder');
        Route::post('submit',           'OrderController@submitOrder');
        Route::get('submit/{id}',       'OrderController@submitSavedOrder');
    });
});

Route::post('register', 'AdminController@apiRegistration');
Route::post('/orders/texture/upload',   'OrderController@uploadTexture');

/**
 * pdf routes
 */
Route::post('/orders/pdf/wish-list', 'OrderController@generateWishListPDF');
Route::post('/orders/pdf/invoice', 'OrderController@generateInvoicePDF');