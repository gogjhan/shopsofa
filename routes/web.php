<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * REDIRECTS
 */
Route::get('home', function() { return view('welcome'); });
Route::get('/', function () { return redirect('/dashboard'); });

/**
 * AUTH ROUTES
 */
Auth::routes();

/**
 * ADMIN PANEL
 */
Route::group(['middleware' => ['auth', 'admin']], function() {
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('', 'AdminController@showDashboard');

        Route::get('users/export-data', ['uses' => 'AdminController@exportUsers', 'as' => 'users.export-data']);
        Route::get('users/{userId}/export-data', ['uses' => 'AdminController@exportUserOrders', 'as' => 'users.orders.export-data']);

        Route::get('users', 'AdminController@showUsers');
        Route::get('users/{userId}', 'AdminController@showUser');

        Route::get('orders', 'AdminController@showOrders');
        Route::get('orders/{orderId}', 'AdminController@showOrder');

        Route::get('users/{userId}/make-sales-manager', 'AdminController@makeSalesManager');
        Route::get('users/{userId}/make-customer', 'AdminController@makeCustomer');
    });
});
