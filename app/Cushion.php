<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cushion extends Model
{
    protected $fillable = [
        'order_id',
        'name',
        'size',
        'quantity',
    ];
}
