<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'mobile',
        'email',
        'password',
        'address',
        'city',
        'country',
        'info1',
        'info2',
        'info3'
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user type
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * Get all user orders
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Return type id by given name
     */
    public static function getUserTypeId($type)
    {
        return Type::where('name', $type)->first()->id;
    }

    /**
     * Check if user is sales manager
     */
    public function isSalesManager()
    {
        $sales_manager_type_id = $this->getUserTypeId('Sales Manager');

        return $sales_manager_type_id === $this->type_id ? true : false;
    }

    /**
     * Get all users of given type
     */
    public function scopeOfType($query, $type)
    {
        $type_id = $this->getUserTypeId($type);
        return $query->where('type_id', $type_id);
    }
}
