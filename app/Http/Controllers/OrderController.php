<?php

namespace App\Http\Controllers;

use App\Cushion;
use App\Mail\Invoice;
use App\Mail\OrderSaved;
use App\Mail\OrderSubmitted;
use App\Mail\Wishlist;
use App\Notifications\InvoiceSubmitted;
use App\Order;
use App\Scene;
use App\Texture;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class OrderController extends Controller
{
    /**
     * Get all orders for user
     *
     * @return JSON
     */
    public function getAllOrders(Request $request)
    {
        $orders = $request->user()->orders()->with(['scenes', 'texture', 'cushions'])->get();

        return Response::json($orders, 200);
    }

    /**
     * Get single order
     *
     * @param $id
     * @return mixed
     */
    public function getSingleOrder(Request $request, $id)
    {
        $order = Order::where('id', $id)->with(['scenes', 'texture', 'cushions'])->first();

        return Response::json($order, 200);
    }

    /**
     * Upload texture return texture id
     *
     * @param Request $request
     * @return Integer texture_id
     */
    public function uploadTexture(Request $request)
    {
        //validation rules
        $validator = Validator::make($request->all(), [
            'texture' =>       'required|image'
        ]);

        if ($validator->fails()) {
            return Response::json(['success' => 'false', 'error' => 'Check input fields, input named texture should contain image file.']);
        }

        $texture_id = Texture::upload($request['texture']);

        return Response::json([
            'success' => 'true',
            'texture_id' => $texture_id
        ]);
    }

    /**
     * Create new order
     *
     * @param Request $request
     * @return Order|bool
     */
    public function createOrder(Request $request)
    {
        //validation rules
        $validator = Validator::make($request->all(), [
            'name' =>       'required|max:255',
            'color' =>      'required|max:255',
            'fabric' =>     'required|max:255',
            'num_seats' =>  'required|max:255'
        ]);

        if ($validator->fails()) {
            return false;
        }

        //create order
        $order = new Order([
            'user_id' =>            $request->user()->id,
            'device_id' =>          $request['device_id'],
            'texture_id' =>         $request['texture_id'],
            'name' =>               $request['name'],
            'color' =>              $request['color'],
            'fabric' =>             $request['fabric'],
            'num_seats' =>          $request['num_seats'],
            'piping' =>             $request['piping'],
            'legs' =>               $request['legs'],
            'foam_density_back' =>  $request['foam_density_back'],
            'foam_density_seat' =>  $request['foam_density_seat'],
            'sofa_quantity' =>      $request['sofa_quantity'],
            'height' =>             $request['height'],
            'width' =>              $request['width'],
            'depth' =>              $request['depth'],
        ]);
        $order->save();

        /**
         * Breadcrumbs (Scene model, unity specific)
         *
         * submit format:
         *      scene1,scene2,scene3
         */
        if (isset($request['scenes'])) :
            $str = explode(',', $request['scenes']);
            foreach($str as $scene) {
                $scenes[] = ['name' => $scene, 'order_id' => $order->id];
            }

            Scene::insert($scenes);
        endif;

        /**
         * Cushions
         *
         * submit format:
         *
         */
        if (isset($request['cushions'])) :
            $cushions = explode(':', $request['cushions']);
            foreach($cushions as $cushion) {
                $cushionAttr = explode(',', $cushion);
                Cushion::create([
                    'order_id' => $order->id,
                    'name' => $cushionAttr[0],
                    'size' => $cushionAttr[1],
                    'quantity' => $cushionAttr[2]
                ]);
            }
        endif;

        return $order;
    }

    /**
     * Add new order to wish list
     *
     * @return Boolean
     */
    public function saveOrder(Request $request)
    {
        $order = $this->createOrder($request);

        //if validation failed
        if(!$order):
            return Response::json(['success' => false, 'error' => 'Check input fields']);
        else:
            $saved_order = Order::where('id', $order->id)->with(['scenes', 'texture', 'cushions'])->first();
            return Response::json(['order' => $saved_order]);
        endif;
    }

    /**
     * Submit new order
     *
     * @return boolean success
     */
    public function submitOrder(Request $request)
    {
        $order = $this->createOrder($request);

        //if validation failed
        if(!$order):
            return Response::json(['success' => false, 'error' => 'Check input fields']);
        else:
            $order->ordered = true;
            $order->save();


            $saved_order = Order::where('id', $order->id)->with(['scenes', 'texture', 'cushions', 'user'])->first();

            return Response::json(['order' => $saved_order]);
        endif;
    }

    /**
     * Submit already saved wish list order
     *
     * @param $orderId
     * @return boolean for success
     */
    public function submitSavedOrder($orderId)
    {
        Order::find($orderId)->update(['ordered' => true]);

        return Response::json(['success' => true], 200);
    }

    public function generateWishListPDF(Request $request)
    {
        $data = [
            'order' => $request,
        ];

        $pdf = PDF::loadView('pdf.wishlist', $data)->stream();

        Mail::to('ifo849otda8285@hpeprint.com')->send(new OrderSaved($pdf));
        Mail::to('ogbye797edo454@hpeprint.com')->send(new OrderSaved($pdf));
        Mail::to('arman_zakaryan@edu.aua.am')->send(new OrderSaved($pdf));
//        Mail::to('gogjhan@gmail.com')->send(new Wishlist($pdf));

        return response()->json([ 'success' => true ]);
    }

    public function generateInvoicePDF(Request $request)
    {
        //build items array
        $items = explode(':', $request['items']);
        $itemsArr = [];
        foreach($items as $item) {
            $itemAttr = explode(',', $item);
            $itemsArr[] = [$itemAttr[0], $itemAttr[1], $itemAttr[2], $itemAttr[3], $itemAttr[4]];
        }
        $request['items'] = $itemsArr;


        $data = [
            'order' => $request
        ];

        $pdf = PDF::loadView('pdf.invoice', $data)->stream();

        Mail::to('ifo849otda8285@hpeprint.com')->send(new OrderSaved($pdf));
        Mail::to('ogbye797edo454@hpeprint.com')->send(new OrderSaved($pdf));
        Mail::to('arman_zakaryan@edu.aua.am')->send(new OrderSaved($pdf));
//        Mail::to('gogjhan@gmail.com')->send(new Invoice($pdf));

        return response()->json([ 'success' => true ]);
    }
}
