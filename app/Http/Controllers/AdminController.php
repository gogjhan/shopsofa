<?php

namespace App\Http\Controllers;

use App\Order;
use App\Type;
use App\User;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    /**
     *
     *
     * VIEWS
     *
     */

    /**
     * Show admin dashboard
     */
    public function showDashboard()
    {
        return view('dashboard');
    }

    /**
     * Show admin users page
     */
    public function showUsers()
    {
        $customers = User::ofType('Customer')->orderBy('id', 'desc')->get();
        $salesManagers = User::ofType('Sales Manager')->orderBy('id', 'desc')->get();

        $data = [
            'customers' => $customers,
            'salesManagers' => $salesManagers
        ];

        return view('dashboard.users')->with($data);
    }

    /**
     * Show admin user info page
     */
    public function showUser($id)
    {
        $user = User::find($id);
        $savedOrders = $user->orders()->savedOrders()->get();
        $submittedOrders = $user->orders()->submittedOrders()->get();

        $data = [
            'user' => $user,
            'savedOrders' => $savedOrders,
            'submittedOrders' => $submittedOrders,
        ];

        return view('dashboard.user')->with($data);
    }

    /**
     * Show admin all orders page
     */
    public function showOrders()
    {
        $submittedOrders = Order::submittedOrders()->with('user')->orderBy('id', 'desc')->get();
        $savedOrders = Order::savedOrders()->with('user')->orderBy('id', 'desc')->get();

        $data = [
            'submittedOrders' => $submittedOrders,
            'savedOrders' => $savedOrders,
        ];

        return view('dashboard.orders')->with($data);
    }

    /**
     * Show admin single order details page
     */
    public function showOrder($id)
    {
        $order = Order::where('id', $id)->with('user')->first();

        $data = [
            'order' => $order,
        ];

        return view('dashboard.order')->with($data);
    }


    /**
     *
     * Actions
     *
     */


    /**
     * Turn user type to Sales Manager
     */
    public function makeSalesManager($id)
    {
        $sales_manager_type_id = Type::where('name', 'Sales Manager')->first()->id;
        User::find($id)->type()->associate($sales_manager_type_id)->save();

        return redirect()->back()->with('message', 'Success');
    }

    /**
     * Turn user type to Customer
     */
    public function makeCustomer($id)
    {
        $customer_type_id = Type::where('name', 'Customer')->first()->id;
        User::find($id)->type()->associate($customer_type_id)->save();

        return redirect()->back()->with('message', 'Success');
    }

    /**
     * Create new user from API
     */
    public function apiRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }

        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'address' => $request['address'],
            'password' => bcrypt($request['password']),
        ]);

        //make user a customer by default
        $customer_type_id = Type::where('name', 'Customer')->first()->id;
        $user->type()->associate($customer_type_id)->save();

        return $user;
    }

    /**
     * Get user object
     */
    public function getUser(Request $request)
    {
        return Response::json($request->user(), 200);
    }

    /**
     * Update user info
     */
    public function editUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);

        if ($validator->fails()) {
            return Response::json(['success' => false, 'error' => 'Check input fields']);
        }

        $request->user()->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'mobile' => $request['mobile'],
            'password' => bcrypt($request['mobile']),
            'address' => $request['address'],
            'city' => $request['city'],
            'country' => $request['country'],
            'info1' => $request['info1'],
            'info2' => $request['info2'],
            'info3' => $request['info3'],
        ]);

        return Response::json($request->user(), 200);
    }

    /**
     * Export user data in excel format
     */
    public function exportUserOrders($userId)
    {
        $user = User::whereId($userId)->select(['id', 'first_name', 'last_name', 'mobile', 'email', 'address', 'created_at'])->get()->toArray();
        $orders = User::find($userId)->orders()->select(['id', 'device_id', 'name', 'color', 'fabric', 'num_seats', 'piping', 'legs', 'foam_density_back', 'foam_density_seat', 'sofa_quantity', 'height', 'width', 'depth', 'ordered', 'created_at'])->get()->toArray();

        Excel::create('orders', function($excel) use($orders, $user) {
            $excel->sheet('User', function($sheet) use($user) {
                $sheet->fromArray($user);
            });
            $excel->sheet('Orders', function($sheet) use($orders) {
                $sheet->fromArray($orders);
            });

        })->export('xls');
    }

    /**
     * export users in excel format
     */
    public function exportUsers() {
        $users = User::where('type_id', '!=' ,1)->select(['id', 'first_name', 'last_name', 'mobile', 'email', 'address', 'created_at'])->get()->toArray();

        Excel::create('users', function($excel) use($users) {
            $excel->sheet('Users', function($sheet) use($users) {
                $sheet->fromArray($users);
            });

        })->export('xls');
    }
}
