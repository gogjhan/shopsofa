<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InvoiceSubmitted extends Notification
{
    use Queueable;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('New order!')
            ->greeting('New order was submitted.')
            ->line('ID: ' . $this->order->id)
            ->line('Name: ' . $this->order->name)
            ->line('Color: ' . $this->order->color)
            ->line('Fabric: ' . $this->order->fabric)
            ->line('No. of seats: ' . $this->order->num_seats)
            ->line('Piping: ' . $this->order->piping)
            ->line('Legs: ' . $this->order->legs)
            ->line('Foam Density Back: ' . $this->order->foam_density_back)
            ->line('Foam Density Seat: ' . $this->order->foam_density_seat)
            ->line('Height: ' . $this->order->height)
            ->line('Width: ' . $this->order->width)
            ->line('Date: ' . $this->order->created_at);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
