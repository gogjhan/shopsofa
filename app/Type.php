<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * There are 3 types of users ["Admin", "Customer", "Sales Manager"]
     */

    protected $fillable = [
        'name',
    ];

    /**
     * Get all users for given type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
