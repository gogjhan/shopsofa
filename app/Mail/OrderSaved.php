<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSaved extends Mailable
{
    use Queueable, SerializesModels;

    public $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->text('emails.order-saved')->attach($this->pdf, [
//            'as' => 'wishlist.pdf',
//            'mime' => 'application/pdf'
//        ]);

        return $this->text('emails.order-saved')->attachData($this->pdf, 'wishlist.pdf', ['mime' => 'application/pdf']);
    }
}
