<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'device_id',
        'name',
        'texture_id',
        'color',
        'fabric',
        'num_seats',
        'piping',
        'legs',
        'foam_density_back',
        'foam_density_seat',
        'sofa_quantity',
        'height',
        'width',
        'depth',
        'ordered',
    ];

    /**
     * Get all submitted orders
     *
     * @param $query
     * @return Order[]
     */
    public function scopeSubmittedOrders($query)
    {
        return $query->where('ordered', 1);
    }

    /**
     * Get all saved for later orders
     *
     * @param $query
     * @return Order[]
     */
    public function scopeSavedOrders($query)
    {
        return $query->where('ordered', 0);
    }

    /**
     * Get order's user
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scenes()
    {
        return $this->hasMany(Scene::class);
    }

    public function texture()
    {
        return $this->belongsTo(Texture::class);
    }

    public function cushions()
    {
        return $this->hasMany(Cushion::class);
    }
}
