<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as ImageManager;

class Texture extends Model
{
    protected $fillable = [
        'source',
    ];

    /**
     * Uploads image and returns image id
     *
     * @param $image
     * @return string
     */
    public static function upload($image)
    {
        //save base image
        $image_name = time() . '_' . mt_rand(1000, 9999) . "." . $image->getClientOriginalExtension();
        $path = base_path() . '/public/images/textures/';
        $image->move($path, $image_name);

        //create thumbnail of image and save
        $thumb = ImageManager::make($path . $image_name);
        $thumb->fit(640, 480)->save($path . 'thumbs/thumb_' . $image_name);

        //create image in database
        $db_image = new Texture(['source' => $image_name]);
        $db_image->save();

        return $db_image->id;
    }
}
